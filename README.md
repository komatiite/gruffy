# README #

Gruffy (code name 'dunite' after the green ultramafic rock) is a mobile mining and exploration arcade game for Android and iOS. Players direct a rotund, little prospector named 'Gruffy' in his efforts to explore different rock units in an underground, stratigraphic section and mine a number of precious metals and gems.


### Features ###

The game features five, real-world deposit models, 20+ ore types, four mining tools, and four exploration methods.


### Specifications ###

Gruffy is programmed in Java using the LibGDX game library.


### About Kent Crozier ###

Hi! I am a former exploration geologist in 2.5+ billion-year-old gold, nickle, and copper terranes with a B.Sc Geological Sciences (Honours) from the University of Manitoba. As of 2016, I am a Business Information Technology student at Red River College in Winnipeg learning Java, C#, .NET, SQL, and Swift. I am interested in mobile app and game development, and scientific and business desktop application development.