package com.komatiite.dunite;

/**
 * Models the pickaxe tool; extends Tool with some tool-specific considerations.
 */
public class Pickaxe extends Tool
{
    public Pickaxe(int cost, int speed, int recovery, int level)
    {
        super("Pickaxe", cost, speed, recovery, level);

        // Load tool attributes and upgrades
        loadUpgrades("pickaxe_upgrades.txt");
    }
}
