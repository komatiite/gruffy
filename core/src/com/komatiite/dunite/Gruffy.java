package com.komatiite.dunite;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;


public class Gruffy implements ApplicationListener, GestureListener
{
	// Fields
	TiledMap level;
	TiledMapRenderer tiledMapRenderer;
	Section section;
	OrthographicCamera camera;
	TextureAtlas gruffyAtlas;
	Animation gruffyAnimation;
	Sprite gruffySprite;
	Texture gruffyTexture;
	SpriteBatch spriteBatch;
	Character gruffy;
	Stage stage;
	Viewport viewport;
	InputMultiplexer inputMultiplexer;
	GestureDetector gestureDetector;
	TextureAtlas menuAtlas;
	Table menuAction;
	Table menuMining;
	Table menuTools;
	Actor actionCancelItem;
	Actor actionWalkItem;
	Actor actionMineItem;
	Actor actionToolItem;
	Actor actionToggleItem;
	Actor mineCancelItem;
	Actor mineLeftItem;
	Actor mineRightItem;
	Actor mineDownItem;
	Actor minePathItem;
	Actor toolCancelItem;
	Actor toolPickaxeItem;
	Actor toolJackhammerItem;
	Actor toolLaserItem;
	Actor toolPlasmaItem;
	Actor toolSelectItem;
	Pickaxe pickaxe;
	Jackhammer jackhammer;
	ArrayList<Tool> tools;


	float elapsedTime = 0;
	float frameTime = 0;
	float miningTime = 0;



	@Override
	public void create()
	{
		// Load the section level into the TiledMap
		level = new TmxMapLoader().load("level_test.tmx");

		// Create the section
		section = new Section(level);

		// Assign the section level to the map renderer
		tiledMapRenderer = new OrthogonalTiledMapRenderer(level);

		// Initialize the orthographic camera
		camera = new OrthographicCamera();

		// Set the projection to top-down
		camera.setToOrtho(false);

		// Position camera within the section
		camera.position.set(section.getStartingX(), section.getStartingY(), 0);

		// Update camera view
		camera.update();

		gruffy = new Character(5, 46, section);

		// Animation stuff
		spriteBatch = new SpriteBatch();
		gruffyTexture = new Texture(Gdx.files.internal("gruffy/gruffy_stopped.png"));
		gruffySprite = new Sprite(gruffyTexture);

		menuAtlas = new TextureAtlas(Gdx.files.internal("menu/Menus.pack"));

		initializeMenus();
		initializeTools();


		stage = new Stage();
		stage.getViewport().setCamera(camera);
		stage.addActor(menuAction);
		stage.addActor(menuMining);
		stage.addActor(menuTools);


		// Setup inputs
		gestureDetector = new GestureDetector(this);
		inputMultiplexer = new InputMultiplexer(stage, gestureDetector);
		Gdx.input.setInputProcessor(inputMultiplexer);

	}

	@Override
	public void dispose() {
		// Dispose map object
		level.dispose();
		gruffyAtlas.dispose();
		spriteBatch.dispose();
		stage.dispose();

	}

	@Override
	public void render ()
	{
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		camera.update();
		tiledMapRenderer.setView(camera);
		tiledMapRenderer.render();


		spriteBatch.setProjectionMatrix(camera.combined);
		spriteBatch.begin();

		elapsedTime += Gdx.graphics.getDeltaTime();
		frameTime += Gdx.graphics.getDeltaTime();
		miningTime += Gdx.graphics.getDeltaTime();

		// Gruffy is walking
		if (gruffy.getState() == Character.State.Walking)
		{
			if (elapsedTime > 0.03)
			{
				gruffy.move();

				elapsedTime = 0;
			}


			gruffyAnimation = gruffy.getCurrentAnimation();

			spriteBatch.draw(gruffyAnimation.getKeyFrame(frameTime, true), gruffy.getPixelX(), gruffy.getPixelY());
		}
		// Gruffy is mining
		else if (gruffy.getState() == Character.State.Mining && Mining.hasPath())
		{
			// Track animation
			if (elapsedTime > 0.03)
			{
				elapsedTime = 0;
			}

			// Get current animation
			gruffyAnimation = gruffy.getCurrentAnimation();

			// Draw animation
			spriteBatch.draw(gruffyAnimation.getKeyFrame(frameTime, true), gruffy.getPixelX(), gruffy.getPixelY());

			// Check how long Gruffy has been mining the unit
			if (miningTime > 2.0)
			{
				// Process the ore in the unit
				Mining.processOre(section);

				// Get Gruff's next move
				gruffy.setPath(Pathfinder.getNextMinePath(Mining.getX(), Mining.getY()));

				// Reset mining time
				miningTime = 0;
			}
		}
		else
		{
			spriteBatch.draw(gruffyTexture, gruffy.getPixelX(), gruffy.getPixelY());
		}



		spriteBatch.end();

		// Updates the location of the visible menu
		if (Menu.checkVisible())
		{
			Table menu = Menu.getVisible();

			menu.setPosition(gruffy.getPixelX() + 32, gruffy.getPixelY() + 128);
		}

		stage.draw();
	}


	@Override
	public void resize(int width, int height)
	{
	}

	@Override
	public void pause()
	{
	}

	@Override
	public void resume()
	{
	}

	// Gesture Listener overrides **********************************************************
	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY)
	{
		// Allow the player to pan camera over the section
		camera.translate(-deltaX, deltaY);

		// Get boundary values
		int leftEdge= section.getCameraLeft();
		int rightEdge = section.getCameraRight();
		int topEdge = section.getCameraTop();
		int bottomEdge = section.getCameraBottom();

		// Check if the camera has touched the left boundary of the section
		if (camera.position.x < leftEdge)
		{
			// Constrain camera to the leftmost boundary
			camera.position.x = leftEdge;
		}
		// Check if the camera has touched the right boundary of the section
		else if (camera.position.x > rightEdge) {
			// Constrain the camera to the rightmost boundary
			camera.position.x = rightEdge;
		}
		// Check if the camera has touched the upper boundary of the section
		if (camera.position.y > topEdge)
		{
			// Constrain the camera to the topmost boundary
			camera.position.y = topEdge;
		}
		// Check if the camera has touched the lower boundary of the section
		else if (camera.position.y < bottomEdge)
		{
			// Constrain the camera to the lowermost boundary
			camera.position.y = bottomEdge;
		}

		// Update the camera view
		camera.update();

		return true;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button)
	{
		int menuItemWidth = section.getUnitWidth();

		// Get the section unit coordinates for the touch down
		SectionPosition position = new SectionPosition(x, y, section, camera);

		// Check if user has activated the action menu by clicking on Gruffy
		if (position.x == gruffy.getSectionX() && position.y == gruffy.getSectionY())
		{
			// Erase action menu if visible
			if (Menu.checkVisible())
			{
				Menu.eraseMenus();
			}
			// Show action menu if no menus are visible
			else
			{
				menuAction.setPosition(gruffy.getSectionX() * menuItemWidth + (menuItemWidth / 2), (gruffy.getSectionY() + 2) * menuItemWidth);
				menuAction.setVisible(true);
			}
		}
		// Check if Gruffy can be moved
		else if (gruffy.getWalkable())
		{
			if (Pathfinder.checkBounds(section, position) && Pathfinder.checkPath(section, position) && !(position.x == gruffy.getSectionX() && position.y == gruffy.getSectionY()))
			{
				if (!gruffy.hasPath())
				{
					// Get and set the path
					gruffy.setPath(Pathfinder.getPath(gruffy, section, position.x, position.y));

					// Erase action menu if visible
					if (Menu.checkVisible())
					{
						Menu.eraseMenus();
					}
				}
			}
		}
		// Erase any outstanding menus
		else
		{
			Menu.eraseMenus();
		}

		return true;
	}

	@Override
	public boolean tap(float x, float y, int count, int button)
	{
		return true;
	}

	@Override
	public boolean longPress(float x, float y)
	{
		return true;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button)
	{
		return true;
	}

	@Override
	public boolean panStop(float x, float y, int deltaX, int deltaY)
	{
		return true;
	}

	@Override
	public boolean zoom(float initialDistance, float distance)
	{
		return true;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
						 Vector2 pointer1, Vector2 pointer2)
	{
		return true;
	}

	/**
	 * Initializes the in-game menus.
	 */
	private void initializeMenus()
	{
		// Build action menu
		actionCancelItem = new MenuItem("cancel", menuAtlas, "menu_action_cancel", "menu_action_continue");
		actionWalkItem = new MenuItem("walk", menuAtlas, "menu_action_walk", "menu_action_cancel");
		actionMineItem = new MenuItem("mine", menuAtlas, "menu_action_mine", "menu_action_cancel");
		actionToolItem = new MenuItem("tool", menuAtlas, "menu_action_tool", "menu_action_cancel");
		actionToggleItem = new MenuItem("toggle", menuAtlas, "menu_action_cancel", "menu_action_continue");

		// Create action menu
		menuAction = new Table();
		menuAction.setVisible(false);
		menuAction.add(actionCancelItem).width(66);
		menuAction.add(actionWalkItem).width(66);
		menuAction.add(actionMineItem).width(66);
		menuAction.add(actionToolItem).width(66);
		menuAction.add(actionToggleItem).width(64);

		// Build mining menu
		mineCancelItem = new MenuItem("cancel", menuAtlas, "menu_action_cancel", "menu_action_continue");
		mineLeftItem = new MenuItem("left", menuAtlas, "menu_mine_left", "menu_action_cancel");
		mineDownItem = new MenuItem("down", menuAtlas, "menu_mine_down", "menu_action_cancel");
		mineRightItem = new MenuItem("right", menuAtlas, "menu_mine_right", "menu_action_cancel");
		minePathItem = new MenuItem("path", menuAtlas, "menu_mine_path", "menu_action_continue");

		// Create action menu
		menuMining = new Table();
		menuMining.setVisible(false);
		menuMining.add(mineCancelItem).width(66);
		menuMining.add(mineLeftItem).width(66);
		menuMining.add(mineDownItem).width(66);
		menuMining.add(mineRightItem).width(66);
		menuMining.add(minePathItem).width(64);

		// Build tools menu
		toolCancelItem = new MenuItem("cancel", menuAtlas, "menu_action_cancel", "menu_action_continue");
		toolPickaxeItem = new MenuItem("pickaxe", menuAtlas, "menu_tools_pickaxe", "menu_action_cancel");
		toolJackhammerItem = new MenuItem("jackhammer", menuAtlas, "menu_tools_jackhammer", "menu_action_cancel");
		toolLaserItem = new MenuItem("laser", menuAtlas, "menu_tools_laser", "menu_action_cancel");
		toolPlasmaItem = new MenuItem("plasma", menuAtlas, "menu_tools_plasma", "menu_action_continue");
		toolSelectItem = new MenuItem("select", menuAtlas, "menu_tools_select", "menu_action_continue");


		// Create  tools menu
		menuTools = new Table();
		menuTools.setVisible(false);
		menuTools.add(toolCancelItem).width(66);
		menuTools.add(toolPickaxeItem).width(66);
		menuTools.add(toolJackhammerItem).width(66);
		menuTools.add(toolLaserItem).width(66);
		menuTools.add(toolPlasmaItem).width(66);
		menuTools.add(toolSelectItem).width(64);

		Menu.setMenus(menuAction, menuMining, menuTools);


		/**
		 * Handles action menu cancel event.
		 */
		actionCancelItem.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				menuAction.setVisible(false);

				return true;
			}
		});

		/**
		 * Handles action menu walk event.
		 */
		actionWalkItem.addListener(new InputListener()
		{
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				gruffy.setWalkable(true);

				menuAction.setVisible(false);

				System.out.println("Walking");

				return true;
			}
		});

		/**
		 * Handles action menu mine event.
		 */
		actionMineItem.addListener(new InputListener()
		{
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				menuAction.setVisible(false);
				menuMining.setPosition(gruffy.getSectionX() * 64 + 32, (gruffy.getSectionY() + 2) * 64);
				menuMining.setVisible(true);

				return true;
			}
		});

		/**
		 * Handles action menu change tool event.
		 */
		actionToolItem.addListener(new InputListener()
		{
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				menuAction.setVisible(false);
				menuTools.setPosition(gruffy.getSectionX() * 64 + 32, (gruffy.getSectionY() + 2) * 64);
				menuTools.setVisible(true);

				return true;
			}
		});

		/**
		 * Handles action menu toggle event.
		 */
		actionToggleItem.addListener(new InputListener()
		{
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				if (gruffy.getState() == Character.State.Mining)
				{
					gruffy.setAction(Character.Action.StandingRight);
				}

				return true;
			}
		});

		/**
		 * Handles mining menu cancel event.
		 */
		mineCancelItem.addListener(new InputListener()
		{
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				menuMining.setVisible(false);
				menuAction.setVisible(true);

				return true;
			}
		});

		/**
		 * Handles mining menu mine left event.
		 */
		mineLeftItem.addListener(new InputListener()
		{
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				menuMining.setVisible(false);

				if (Mining.getMinePath(section, gruffy, Mining.Direction.Left))
				{
					gruffy.setAction(Character.Action.MiningLeft);
					miningTime = 0;
				}
				else
				{
					System.out.println("No rock to the left!");
				}

				return true;
			}
		});

		/**
		 * Handles mining menu mine down event.
		 */
		mineDownItem.addListener(new InputListener()
		{
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				menuMining.setVisible(false);

				if (Mining.getMinePath(section, gruffy, Mining.Direction.Down))
				{
					gruffy.setAction(Character.Action.MiningDown);
					miningTime = 0;
				}
				else
				{
					System.out.println("No rock below!");
				}

				return true;
			}
		});

		/**
		 * Handles mining menu mine right event.
		 */
		mineRightItem.addListener(new InputListener()
		{
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				menuMining.setVisible(false);

				if (Mining.getMinePath(section, gruffy, Mining.Direction.Right))
				{
					gruffy.setAction(Character.Action.MiningRight);
					miningTime = 0;
				}
				else
				{
					System.out.println("No rock to the right!");
				}

				return true;
			}
		});

		/**
		 * Handles tool menu cancel event.
		 */
		toolCancelItem.addListener(new InputListener()
		{
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				menuTools.setVisible(false);
				menuAction.setVisible(true);

				return true;
			}
		});


	}

	public void initializeTools()
	{
		// Initialize pickaxe
		pickaxe = new Pickaxe(10, 10, 60, 0);

		// Initialize jackhammer
		jackhammer = new Jackhammer(10, 40, 65, 5);

		// Initialize tool list
		tools = new ArrayList<Tool>();

		tools.add(pickaxe);
		tools.add(jackhammer);

		Mining.setTool(pickaxe);
	}
}