package com.komatiite.dunite;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Models the tools used by the character.
 */
public abstract class Tool
{
    // Upgrade lists
    private ArrayList<Upgrade> costUpgrade;
    private ArrayList<Upgrade> speedUpgrade;
    private ArrayList<Upgrade> recoveryUpgrade;

    // Tool attributes
    private String name;
    private int cost;
    private int speed;
    private int recovery;
    private int level;

    /**
     * Constructs the tool object.
     * @param name The name of the tool.
     * @param cost The cost of using the tool.
     * @param speed The speed at which the tool mines ore.
     * @param recovery The amount of ore the tool recovers (as a percentage).
     * @param level The character level at which the tool is unlocked.
     */
    public Tool(String name, int cost, int speed, int recovery, int level)
    {
        this.name = name;
        this.cost = cost;
        this.speed = speed;
        this.recovery = recovery;
        this.level = level;
    }

    /**
     * Gets the cost of the tool.
     * @return The cost value.
     */
    public int getCost()
    {
        int costBonus = 0;

        // Cycle through upgrade list
        for (Upgrade upgrade : costUpgrade)
        {
            // If current upgrade is active, add value to bonus
            if (upgrade.active)
            {
                costBonus += upgrade.value;
            }
        }

        return cost + costBonus;
    }

    /**
     * Gets the speed of the tool.
     * @return The speed value.
     */
    public int getSpeed()
    {
        int speedBonus = 0;

        // Cycle through upgrade list
        for (Upgrade upgrade : speedUpgrade)
        {
            // If current upgrade is active, add value to bonus
            if (upgrade.active)
            {
                speedBonus += upgrade.value;
            }
        }

        return speed + speedBonus;
    }

    /**
     * Gets the recovery of the tool.
     * @return The recovery value.
     */
    public int getRecovery()
    {
        int recoveryBonus = 0;

        // Cycle through upgrade list
        for (Upgrade upgrade : recoveryUpgrade)
        {
            // If current upgrade is active, add value to bonus
            if (upgrade.active)
            {
                recoveryBonus += upgrade.value;
            }
        }

        return recovery + recoveryBonus;
    }

    /**
     * Gets the level at which the tool unlocks.
     * @return The level number.
     */
    public int getLevel()
    {
        return level;
    }

    /**
     * Gets the upgrades that are available to be purchased by the player.
     * @param type The type of the upgrade to get; cost, speed, or recovery.
     * @param characterLevel The current level of the character (upgrades restricted by level).
     * @return The list of the available upgrades for teh upgrade type.
     */
    public ArrayList<Upgrade> getAvailableUpgrades(Upgrade.UpgradeType type, int characterLevel)
    {
        // Declare and initialize lists
        ArrayList<Upgrade> availableUpgrades = new ArrayList<Upgrade>();
        ArrayList<Upgrade> searchList = new ArrayList<Upgrade>();

        // Determine which upgrade list to search
        switch(type)
        {
            case Cost:
                searchList = costUpgrade;
                break;
            case Speed:
                searchList = speedUpgrade;
                break;
            case Recovery:
                searchList = recoveryUpgrade;
                break;
        }

        // Cycle through upgrade list
        for (Upgrade upgrade : searchList)
        {
            // If the character has a high enough level, add the upgrade to the list
            if (upgrade.level <= characterLevel)
            {
                availableUpgrades.add(upgrade);
            }
        }

        return availableUpgrades;
    }

    /**
     * Loads upgrade data into upgrade lists for specific tool.
     * @param filename The ame of the file to load.
     */
    public void loadUpgrades(String filename)
    {
        // Initialize upgrade lists
        costUpgrade = new ArrayList<Upgrade>();
        speedUpgrade = new ArrayList<Upgrade>();
        recoveryUpgrade = new ArrayList<Upgrade>();

        // Build path to file
        String path = String.format("%s%s", "tools\\", filename);

        try
        {
            // Create scanner object
            Scanner reader = new Scanner(new File(path));
            reader.useDelimiter(",");

            // Create temporary placeholders
            Upgrade.UpgradeType type;
            String name;
            int price;
            int value;
            int level;

            // Cycle through lines in file
            while(reader.hasNext())
            {
                // Prepare data
                type = Upgrade.UpgradeType.valueOf(reader.next().trim());
                name = reader.next();
                price = Integer.parseInt(reader.next());
                value = Integer.parseInt(reader.next());
                level = Integer.parseInt(reader.next());

                // Create upgrade object
                Upgrade upgrade = new Upgrade(type, name, price, value, level);

                // Add upgrade object to appropriate list
                switch (upgrade.type)
                {
                    case Cost:
                        costUpgrade.add(upgrade);
                        break;
                    case Speed:
                        speedUpgrade.add(upgrade);
                        break;
                    case Recovery:
                        recoveryUpgrade.add(upgrade);
                        break;
                }
            }
        }
        catch (FileNotFoundException e)
        {
            System.out.println("Error loading " + path);
            System.out.println(e.getStackTrace().toString());
        }
    }
}
