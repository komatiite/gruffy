package com.komatiite.dunite;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import java.util.ArrayList;

/**
 * Created by kcroz on 2016-07-04.
 */
public class Menu
{
    private static ArrayList<Table> menus;

    /**
     * Adds the game's menus to a list.
     * @param action The action menu.
     * @param mining The mining menu.
     * @param tools The tools menu.
     */
    public static void setMenus(Table action, Table mining, Table tools)
    {
        menus = new ArrayList<Table>();
        menus.add(action);
        menus.add(mining);
        menus.add(tools);
    }

    /**
     * Turns off any currently visible menus.
     */
    public static void eraseMenus()
    {
        for (Table menu : menus)
        {
            if (menu.isVisible())
            {
                menu.setVisible(false);
            }
        }
    }

    /**
     * Checks if any menu is currently visible.
     * @return The true or false determination if a menu is visible (true) or not (false).
     */
    public static boolean checkVisible()
    {
        boolean visible = false;

        for (Table menu : menus)
        {
            if (menu.isVisible())
            {
                visible = true;
            }
        }

        return visible;
    }

    /**
     * Gets the currently visible menu.
     * @return
     */
    public static Table getVisible()
    {
        Table visibleMenu = null;

        for (Table menu : menus)
        {
            if (menu.isVisible())
            {
                visibleMenu = menu;
            }
        }

        return visibleMenu;
    }


}
