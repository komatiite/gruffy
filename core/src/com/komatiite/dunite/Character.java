package com.komatiite.dunite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import java.util.ArrayList;

/**
 * Models character movement, state, and action throughout the section.
 */
public class Character
{
    /**
     * Type of character actions.
     */
    public enum Action
    {
        StandingRight,
        StandingLeft,
        Scratching,
        Sleeping,
        WalkingRight,
        WalkingLeft,
        ClimbingUp,
        ClimbingDown,
        Falling,
        MiningLeft,
        MiningRight,
        MiningDown
    }

    public enum State
    {
        Stationary,
        Walking,
        Mining
    }

    // Fields
    private Section section;
    private ArrayList<Node> path;
    private Node previousNode;
    private float xPixel;
    private float yPixel;
    private int xPixelBuffer;
    private int yPixelBuffer;
    private int xSection;
    private int ySection;
    private Action action;
    private ArrayList<Action> actionList;
    private State state;
    private int speed;
    private boolean walkable;

    // Texture Atlases
    TextureAtlas walkingLeftTexture;
    TextureAtlas walkingRightTexture;
    TextureAtlas climbingUpTexture;
    TextureAtlas climbingDownTexture;
    TextureAtlas miningLeftTexture;
    TextureAtlas miningRightTexture;
    TextureAtlas miningDownTexture;

    // Animations
    Animation currentAnimation;
    Animation walkingLeft;
    Animation walkingRight;
    Animation climbingUp;
    Animation climbingDown;
    Animation miningLeft;
    Animation miningRight;
    Animation miningDown;

    /**
     * Constructs the character object.
     * @param x The x coordinate position of the character in the section.
     * @param y The y coordinate position of the character in the section.
     */
    public Character(int x, int y, Section section)
    {
        // Initialize fields
        this.section = section;
        xSection = x;
        ySection = y;
        xPixel = xSection * section.getUnitWidth();
        yPixel = ySection * section.getUnitHeight();
        xPixelBuffer = 0;
        yPixelBuffer = 0;
        action = Action.StandingRight;
        actionList = new ArrayList<Action>();
        state = State.Stationary;
        previousNode = null;
        speed = 4;
        walkable = true;

        // Load texture atlases
        walkingLeftTexture = new TextureAtlas(Gdx.files.internal("gruffy/walking_left.atlas"));
        walkingRightTexture = new TextureAtlas(Gdx.files.internal("gruffy/walking_right.atlas"));
        climbingUpTexture = new TextureAtlas(Gdx.files.internal("gruffy/climbing_up.atlas"));
        climbingDownTexture = new TextureAtlas(Gdx.files.internal("gruffy/climbing_down.atlas"));
        miningLeftTexture = new TextureAtlas((Gdx.files.internal("gruffy/mining_left.atlas")));
        miningRightTexture = new TextureAtlas((Gdx.files.internal("gruffy/mining_right.atlas")));
        miningDownTexture = new TextureAtlas((Gdx.files.internal("gruffy/mining_down.atlas")));


        // Initialize animations
        walkingLeft = new Animation(1/8f, walkingLeftTexture.getRegions());
        walkingRight = new Animation(1/8f, walkingRightTexture.getRegions());
        climbingUp = new Animation(1/8f, climbingUpTexture.getRegions());
        climbingDown = new Animation(1/8f, climbingDownTexture.getRegions());
        miningLeft = new Animation(1/8f, miningLeftTexture.getRegions());
        miningRight = new Animation(1/8f, miningRightTexture.getRegions());
        miningDown = new Animation(1/8f, miningDownTexture.getRegions());

    }


    /**
     * Gets the x coordinate value of the character in section units.
     * @return The x coordinate value.
     */
    public int getSectionX()
    {
        return this.xSection;
    }

    /**
     * Gets the y coordinate value of the character in section units.
     * @return The y coordinate value.
     */
    public int getSectionY()
    {
        return this.ySection;
    }

    /**
     * Gets the x coordinate value of the character in pixels.
     * @return The x coordinate value.
     */
    public float getPixelX()
    {
        return xPixel;
    }

    /**
     * Gets the y coordinate value of the character in pixels.
     * @return The y coordinate value.
     */
    public float getPixelY()
    {
        return yPixel;
    }

    /**
     * Sets the x pixel coordinate of the character.
     * @param x The pixel coordinate to set.
     */
    public void setPixelX(float x)
    {
        this.xPixel = x;
    }

    /**
     * Sets the y pixel coordinate of the character.
     * @param y The pixel to set.
     */
    public void setPixelY(float y)
    {
        this.yPixel = y;
    }

    /**
     * Updates the character's section coordinates based on current pixel coordinate location.
     */
    public void updateSectionCoordinates()
    {
        xSection = (int)getPixelX() / section.getUnitWidth();
        ySection = (int)getPixelY() / section.getUnitHeight();
    }

    /**
     * Gets the state of the character - true is moving, false is stopped.
     * @return The character state.
     */
    public State getState()
    {
        return state;
    }

    public void setState(State state)
    {
        this.state = state;

        if (this.state == State.Mining)
        {
            setWalkable(false);
        }
    }

    public Action getPreviousAction()
    {
        return actionList.get(0);
    }

    /**
     * Determines if character can be set walking on a path, or if they are mining.
     * @return
     */
    public boolean getWalkable()
    {
        return walkable;
    }

    public void setWalkable(boolean walkable)
    {
        this.walkable = walkable;
    }

    /**
     * Checks if the character object already has a working path. If path exists, it is prematurely
     * ended by assigning the current node (i.e. previousNode) to the sole remaining path.
     * @return The true or false value of whether a path exists or not.
     */
    public boolean hasPath()
    {
        boolean isPath = false;

        // Check if there is a path
        if (path != null)
        {
            isPath = true;

            // Reset path
            path.clear();
            path.add(previousNode);
        }

        return isPath;
    }

    /**
     * Sets the path for the character to follow.
     * @param nodePath A list of nodes representing the path.
     */
    public void setPath(ArrayList<Node> nodePath)
    {
        // Initialize path
        this.path = nodePath;

        // Get first node
        Node node = path.get(0);

        //System.out.println("node: " + node.getX() + ", " + node.getY());

        // Figure out which direction the character will go
        // Test of the character's pixel location is at some value > 0 between section units so that
        // move() can smoothly clear out the difference before selecting the first node
        if (xPixelBuffer > 0 || yPixelBuffer > 0)
        {
            // Walk left
            if (node.getX() * section.getUnitWidth() < this.getPixelX())
            {
                setAction(Action.WalkingLeft);
            }
            // Walk right
            else if (node.getX() * section.getUnitWidth() > this.getPixelX())
            {
                setAction(Action.WalkingRight);
            }
            // Climb up
            else if (node.getY() * section.getUnitHeight() > this.getPixelY())
            {
                setAction(Action.ClimbingUp);
            }
            // Climb down
            else if (node.getY() * section.getUnitHeight() < this.getPixelY())
            {
                setAction(Action.ClimbingDown);
            }
        }
        // If the character's pixel location is zeroed to section unit, set buffers to grab first node in move()
        else
        {
            // Walk left
            if (node.getX() < this.getSectionX())
            {
                setAction(Action.WalkingLeft);

                xPixelBuffer = 0;
            }
            // Walk right
            else if (node.getX() > this.getSectionX())
            {
                setAction(Action.WalkingRight);

                xPixelBuffer = 64;
            }
            // Climb up
            else if (node.getY() > this.getSectionY())
            {
                setAction(Action.ClimbingUp);

                yPixelBuffer = 64;
            }
            // Climb down
            else if (node.getY() < this.getSectionY())
            {
                setAction(Action.ClimbingDown);

                yPixelBuffer = 0;
            }
        }
    }

    /**
     * Gets the current action of the character.
     * @return The current action.
     */
    public Action getAction()
    {
        return action;
    }

    /**
     * Sets the current action of the character.
     * @param action The current action.
     */
    public void setAction(Action action)
    {
        // Add previous action to the action list
        actionList.add(this.action);

        // If more than two actions stored, delete the oldest
        if (actionList.size() > 2)
        {
            actionList.remove(0);
        }

        //System.out.println("previous: " + actionList.get(0));
        //System.out.println("current: " + this.action);
        //System.out.println("set to:  " + action);
        //System.out.println("");



        // Update action
        this.action = action;

        // Set the character state to stationary if they are standing
        if (this.action == Action.StandingLeft || this.action == Action.StandingRight)
        {
            // Update section coordinates
            updateSectionCoordinates();

            state = State.Stationary;

            setWalkable(true);
        }
        // Set the character state to walking if they are walking
        else if (this.action == Action.WalkingLeft || this.action == Action.WalkingRight || this.action == Action.ClimbingUp || this.action == Action.ClimbingDown)
        {
            state = State.Walking;

            // Update which animation is currently being used
            updateAnimation();
        }
        else if (this.action == Action.MiningLeft || this.action == Action.MiningRight || this.action == Action.MiningDown)
        {
            state = State.Mining;
            setWalkable(false);

            // Update which animation is currently being used
            updateAnimation();
        }
    }

    /**
     * Determines how the character walks along the found path. Interpolates the movement within
     * section units so that the character is not jumping erratically from tile to tile. Smoothing
     * occurs in pixel steps as defined by 'speed'.
     */
    public void move()
    {
        // Check if there are any remaining pixels in the pixel buffers so that the unfinished
        // character movement of the previous path can be cleared out and re-zeroed to the
        // 0, 0 location of the current tile.
        if (action == Action.WalkingLeft && xPixelBuffer > 0)
        {
            xPixelBuffer -= speed;
            setPixelX(getPixelX() - speed);
        }
        else if (action == Action.WalkingRight && xPixelBuffer < 64)
        {
            xPixelBuffer += speed;
            setPixelX(getPixelX() + speed);
        }
        else if (action == Action.ClimbingDown && yPixelBuffer > 0)
        {
            yPixelBuffer -= speed;
            setPixelY(getPixelY() - speed);
        }
        else if (action == Action.ClimbingUp && yPixelBuffer < 64)
        {
            yPixelBuffer += speed;
            setPixelY(getPixelY() + speed);
        }
        // If the pixel buffers are clear, select the first node in the path list and charge the buffers
        else
        {
            // Check if there are any more nodes in the path list
            if (path.size() > 0)
            {
                // Get current node
                Node node = path.get(0);

                // Update section coordinates
                updateSectionCoordinates();

                // If there is no previous node, create a dummy with the character's position
                if (previousNode == null)
                {
                    previousNode = new Node(null, getSectionX(), getSectionY(), 0, 0);
                }

                // Check for any action changes
                checkActionChange(node);

                // Transition current node to previous for next iteration
                previousNode = node;

                // Remove current node so that the next can be selected
                path.remove(0);
            }
            // No more nodes to process, set character to inactive
            else
            {
                // Set character state based on previous action
                if ((actionList.get(0) == Action.MiningLeft || actionList.get(0) == Action.MiningDown || actionList.get(0) == Action.MiningRight) && Mining.hasPath())
                {
                    setState(State.Mining);
                    setAction(actionList.get(0));
                }
                else
                {
                    setInactive();
                }


                // Reset pixel buffers so that setPath() does not mistake the character's position
                // as being somewhere within a section unit. This block is executed when pixels
                // relative to the current unit are at 0, 0.
                xPixelBuffer = 0;
                yPixelBuffer = 0;
            }
        }
    }

    /**
     * Checks whether or not to change the character's action based on the spatial relationship of
     * the current node with the previous node.
     * @param node
     */
    private void checkActionChange(Node node)
    {
        // Walk left
        if (node.getX() < previousNode.getX())
        {
            setAction(Action.WalkingLeft);

            xPixelBuffer = section.getUnitWidth();
        }
        // Walk right
        else if (node.getX() > previousNode.getX())
        {
            setAction(Action.WalkingRight);

            xPixelBuffer = 0;
        }
        // Climb up
        else if (node.getY() > previousNode.getY())
        {
            setAction(Action.ClimbingUp);

            yPixelBuffer = 0;
        }
        // Climb down
        else if (node.getY() < previousNode.getY())
        {
            setAction(Action.ClimbingDown);

            yPixelBuffer = section.getUnitHeight();
        }
    }

    /**
     * Set the character action to inactive.
     */
    private void setInactive()
    {
        Action restAction;

        if (action == Action.StandingLeft)
        {
            restAction = Action.StandingRight;
        }
        else if (action == Action.WalkingRight)
        {
            restAction = Action.StandingLeft;
        }
        // TO-DO: temporary rest action state until climbing stops are programmed
        else
        {
            restAction = Action.StandingRight;
        }

        // Set action
        setAction(restAction);

        // Erase previous node
        previousNode = null;

        // Erase previous path
        path = null;
    }

    /**
     * Updates the current animation to use based on the character's  current action.
     */
    public void updateAnimation()
    {
        switch(action)
        {
            case WalkingLeft:
                currentAnimation = walkingLeft;
                break;
            case WalkingRight:
                currentAnimation = walkingRight;
                break;
            case ClimbingUp:
                currentAnimation = climbingUp;
                break;
            case ClimbingDown:
                currentAnimation = climbingDown;
                break;
            case MiningLeft:
                currentAnimation = miningLeft;
                break;
            case MiningRight:
                currentAnimation = miningRight;
                break;
            case MiningDown:
                currentAnimation = miningDown;
                break;
        }
    }

    /**
     * Gets the current animation.
     * @return The animation object.
     */
    public Animation getCurrentAnimation()
    {
        return currentAnimation;
    }
}