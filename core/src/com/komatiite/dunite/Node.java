package com.komatiite.dunite;

/**
 * Creates a node object for use in the Pathfinder class.
 */
public class Node
{
    // Fields
    private Node parentNode;
    private int x;
    private int y;
    private int hCost;
    private int gCost;
    private int fCost;

    /**
     * Constructs the node object.
     * @param parentNode The parent node of this node.
     * @param xNode The x coordinate location of this node.
     * @param yNode The y coordinate location of this node.
     * @param xTarget The x coordinate location of the target destination.
     * @param yTarget The y coordinate location of the target destination.
     */
    public Node(Node parentNode, int xNode, int yNode, int xTarget, int yTarget)
    {
        this.parentNode = parentNode;
        this.x = xNode;
        this.y = yNode;
        this.hCost = Math.abs(xTarget - xNode) + Math.abs(yTarget - yNode);
        this.gCost = 10;
        this.fCost = gCost + hCost;
    }

    /**
     * Gets the x coordinate location of the node.
     * @return The x coordinate.
     */
    public int getX()
    {
        return this.x;
    }

    /**
     * Gets the y coordinate location of the node.
     * @return The y coordinate.
     */
    public int getY()
    {
        return this.y;
    }

    /**
     * Gets the fCost of the node.
     * @return The fCost.
     */
    public int getFCost()
    {
        return this.fCost;
    }

    /**
     * Gets the parent node of the node.
     * @return The parent node.
     */
    public Node getParent()
    {
        return parentNode;
    }
}
