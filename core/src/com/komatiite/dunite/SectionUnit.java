package com.komatiite.dunite;

/**
 * Implements the data associated with each unit.
 * Currently only blocking state of the tile is implemented.
 * Future changes will implement ore and lithology values.
 */
public class SectionUnit
{
    private int gold;
    private boolean state;

    /**
     * Constructs the section unit object.
     * @param state The inital true or false blocking state.
     */
    public SectionUnit(boolean state)
    {
        this.state = state;
        gold = 100;
    }

    /**
     * Sets the blocking state.
     * @param state The boolean value to change the state to.
     */
    public void setState(boolean state)
    {
        this.state = state;
    }

    /**
     * Gets the blocking state for the section unit.
     * @return
     */
    public boolean getState()
    {
        return this.state;
    }

    public int getGold()
    {
        return gold;
    }
}
