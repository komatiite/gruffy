package com.komatiite.dunite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Utility class that handles the character pathfinding.
 */
public class Pathfinder
{
    // Fields
    private static ArrayList<Node> openNodes;
    private static ArrayList<Node> closedNodes;
    private static ArrayList<Node> nodePath;

    /**
     * Determines best path from point A to B using the A* pathfinding algorithm.
     * @param character The character object.
     * @param section The stratigraphic section.
     * @param xTarget The x coordinate of the target destination.
     * @param yTarget The y coordinate of the target destination.
     * @return The shortest path found.
     */
    public static ArrayList<Node> getPath(Character character, Section section, int xTarget, int yTarget)
    {
        // Initialize lists
        openNodes = new ArrayList<Node>();
        closedNodes = new ArrayList<Node>();
        nodePath = new ArrayList<Node>();

        // Get character's position
        int baseX = character.getSectionX();
        int baseY = character.getSectionY();

        // Create base node
        Node baseNode = new Node(null, baseX, baseY, xTarget, yTarget);

        // Create current node
        Node currentNode = new Node(baseNode, baseX, baseY, xTarget, yTarget);

        // Loop while the current node's coordinates do not match the target's
        while (currentNode.getX() != xTarget || currentNode.getY() != yTarget)
        {
            // Add the current node to the closed list
            closedNodes.add(currentNode);

            // Get current node coordinates
            int currentX = currentNode.getX();
            int currentY = currentNode.getY();

            // Check if the current node's coordinates fall into the section bounds
            if (currentX > 1 && currentX < section.getSectionWidth() - 1 && currentY > 1 && currentY < section.getSectionHeight() - 1)
            {
                // Check to the left of the current node
                if (!section.getBlockingState(currentX - 1, currentY))
                {
                    boolean nodeExists = false;

                    // Create new potential node
                    Node node = new Node(currentNode, currentX - 1, currentY, xTarget, yTarget);

                    // Check to see if it is on the closed list
                    for (Node nodeToCheck : closedNodes)
                    {
                        if (node.getX() == nodeToCheck.getX() && node.getY() == nodeToCheck.getY())
                        {
                            nodeExists = true;
                        }
                    }

                    if (!nodeExists)
                    {
                        // Add node to the open list
                        openNodes.add(node);
                    }
                }

                // Check above the current node
                if (!section.getBlockingState(currentX, currentY + 1))
                {
                    boolean nodeExists = false;

                    // Create new potential node
                    Node node = new Node(currentNode, currentX, currentY + 1, xTarget, yTarget);

                    // Check to see if it is on the closed list
                    for (Node nodeToCheck : closedNodes)
                    {
                        if (node.getX() == nodeToCheck.getX() && node.getY() == nodeToCheck.getY())
                        {
                            nodeExists = true;
                        }
                    }

                    // Check to see if it is on the closed list
                    if (!nodeExists)
                    {
                        // Add node to the open list
                        openNodes.add(node);
                    }
                }

                // Check to the right of the current node
                if (!section.getBlockingState(currentX + 1, currentY))
                {
                    boolean nodeExists = false;

                    // Create new potential node
                    Node node = new Node(currentNode, currentX + 1, currentY, xTarget, yTarget);

                    // Check to see if it is on the closed list
                    for (Node nodeToCheck : closedNodes)
                    {
                        if (node.getX() == nodeToCheck.getX() && node.getY() == nodeToCheck.getY())
                        {
                            nodeExists = true;
                        }
                    }

                    // Check to see if it is on the closed list
                    if (!nodeExists)
                    {
                        // Add node to the open list
                        openNodes.add(node);
                    }
                }

                // Check below the current node
                if (!section.getBlockingState(currentX, currentY - 1))
                {
                    boolean nodeExists = false;

                    // Create new potential node
                    Node node = new Node(currentNode, currentX, currentY - 1, xTarget, yTarget);

                    // Check to see if it is on the closed list
                    for (Node nodeToCheck : closedNodes)
                    {
                        if (node.getX() == nodeToCheck.getX() && node.getY() == nodeToCheck.getY())
                        {
                            nodeExists = true;
                        }
                    }

                    // Check to see if it is on the closed list
                    if (!nodeExists)
                    {
                        // Add node to the open list
                        openNodes.add(node);
                    }
                }
            }

            // Sort the open list by f cost
            Collections.sort(openNodes, new Comparator<Node>() {
                public int compare(Node one, Node two) {
                    return Integer.compare(one.getFCost(), two.getFCost());
                }
            });

            // Set current node to node with lowest fCost
            currentNode = openNodes.get(0);

            // Remove node from open list
            openNodes.remove(0);
        }

        // Add target node to the final list
        nodePath.add(currentNode);

        // Cycle back through the closed list to get direct path
        while (currentNode.getParent() != null)
        {
            nodePath.add(currentNode.getParent());

            currentNode = currentNode.getParent();
        }

        // Reverse the list
        Collections.reverse(nodePath);

        // Clean first two entries
        nodePath.remove(0);
        nodePath.remove(0);

        return nodePath;
    }

    public static ArrayList<Node> getNextMinePath(int x, int y)
    {
        // Initialize node path
        nodePath = new ArrayList<Node>();

        // Create new node
        Node node = new Node(null, x, y, 0, 0);

        // Add node to the list
        nodePath.add(node);

        return nodePath;
    }

    /**
     * Checks target destination blocking state to see if a path can be derived.
     * @param section The section.
     * @param position The section position object holding the x, y coordinate data.
     * @return The true or false status of whether or not a path can be constructed.
     */
    public static boolean checkPath(Section section, SectionPosition position)
    {
        // Return the opposite of the blocking state (e.g. if blocking is true, check path is false
        return !section.getBlockingState(position.x, position.y);
    }

    /**
     * Checks whether a target destination falls within the section bounds.
     * @param section The section.
     * @param position The position object holding x, y coordinate data.
     * @return The true or false determination.
     */
    public static boolean checkBounds(Section section, SectionPosition position)
    {
        boolean bounds = false;

        if (position.x > 0 && position.x < section.getSectionWidth() - 1 && position.y > 0 && position.y < section.getSectionHeight() - 1)
        {
            bounds = true;
        }

        return bounds;
    }
}
