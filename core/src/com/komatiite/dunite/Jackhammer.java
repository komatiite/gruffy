package com.komatiite.dunite;

/**
 * Models the jackhammer tool; extends Tool with some tool-specific considerations.
 */
public class Jackhammer extends Tool
{
    public Jackhammer(int cost, int speed, int recovery, int level)
    {
        super("Jackhammer", cost, speed, recovery, level);

        loadUpgrades("jackhammer_upgrades.txt");
    }
}
