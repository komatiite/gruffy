package com.komatiite.dunite;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by kcroz on 2016-06-30.
 */
public class MenuItem extends Actor
{
    // Fields
    TextureAtlas itemAtlas;
    TextureRegion itemInactive;
    TextureRegion itemActive;

    public MenuItem(String name, TextureAtlas textureAtlas, String inactive, String active)
    {
        // Initialize fields
        super.setName(name);
        //super.setPosition(0, 0);
        //super.setPosition();
        super.setVisible(true);
        this.setBounds(0, 0, 64, 64);
        this.itemAtlas = textureAtlas;
        this.itemInactive =  this.itemAtlas.findRegion(inactive);
        this.itemActive = this.itemAtlas.findRegion(active);

        //System.out.println(character.getSectionX() * 64 + ", " + (character.getSectionY() + 2) * 64);
    }

    public void draw(Batch batch, float alpha){
        batch.draw(itemInactive, getX(), getY());
    }
}
