package com.komatiite.dunite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;

/**
 * Models the stratigraphic section of the game by manipulating the tiled map.
 */
public class Section
{
    public enum UnitType
    {
        Tunnel,
        Sky
    }

    // Fields
    private TiledMap section;
    private int screenWidth;
    private int screenHeight;
    private int sectionWidth;
    private int sectionHeight;
    private int unitWidth;
    private int unitHeight;
    private int cameraBoundsLeft;
    private int cameraBoundsRight;
    private int cameraBoundsTop;
    private int cameraBoundsBottom;
    private TiledMapTileLayer lithology;
    private TiledMapTileLayer fog;
    private TiledMapTileLayer activity;
    private TiledMapTileLayer character;
    private SectionUnit[][] sectionUnit;

    /**
     * Constructs the the section object.
     * @param map The TiledMap object that represents the loaded stratigraphic .TMX file.
     */
    public Section(TiledMap map)
    {
        // Set map
        this.section = map;

        // Set screen height and width
        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();

        // Get map properties
        MapProperties sectionProperties = map.getProperties();

        // Set the section height and width
        sectionWidth = sectionProperties.get("width", Integer.class);
        sectionHeight = sectionProperties.get("height", Integer.class);

        // Set the unit size
        unitWidth = sectionProperties.get("tilewidth", Integer.class);
        unitHeight = sectionProperties.get("tileheight", Integer.class);

        // Set the section boundaries
        setSectionBoundaries();

        // Get and set map layers
        setMapLayers();

        // Initialize section units
        initializeSectionUnits();
    }

    /**
     * Gets the width of the screen in pixels.
     * @return The screen width in pixels.
     */
    public int getScreenWidth()
    {
        return screenWidth;
    }

    /**
     * Gets the screen height in pixels.
     * @return The screen height in pixels.
     */
    public int getScreenHeight()
    {
        return screenHeight;
    }

    /**
     * Gets the width of the section in unit tiles.
     * @return The section width.
     */
    public int getSectionWidth()
    {
        return sectionWidth;
    }

    /**
     * Gets the height of the section in unit tiles.
     * @return The section height.
     */
    public int getSectionHeight()
    {
        return sectionHeight;
    }

    /**
     * Gets the width of the unit tile in pixels.
     * @return The unit width.
     */
    public int getUnitWidth()
    {
        return unitWidth;
    }

    /**
     * Gets the height of the unit tile in pixels.
     * @return The unit height.
     */
    public int getUnitHeight()
    {
        return unitHeight;
    }

    /**
     * Gets the starting X position for the camera view.
     * @return The starting X position.
     */
    public int getStartingX()
    {
        return getScreenWidth() / 2;
    }
    /**
     * Gets the starting top position for the camera view.
     * @return The starting Y value.
     */
    public int getStartingY()
    {
        return (getSectionHeight() * getUnitHeight()) - (getScreenHeight() / 2);
    }

    /**
     * Sets the camera boundaries for the section.
     */
    public void setSectionBoundaries() {

        cameraBoundsLeft = getStartingX();
        cameraBoundsRight = (getSectionWidth() * getUnitWidth()) - getStartingX();
        cameraBoundsTop = getStartingY();
        cameraBoundsBottom = getScreenHeight()/2;

    }

    /**
     * Gets the leftmost boundary for the camera.
     * @return The leftmost edge.
     */
    public int getCameraLeft() {
        return cameraBoundsLeft;
    }

    /**
     * Gets the rightmost boundary for the camera.
     * @return The rightmost edge.
     */
    public int getCameraRight() {
        return cameraBoundsRight;
    }

    /**
     * Gets the uppermost boundary for the camera.
     * @return The uppermost edge.
     */
    public int getCameraTop() {
        return cameraBoundsTop;
    }

    /**
     * Gets the lowermost boundary for the camera.
     * @return The lowermost edge.
     */
    public int getCameraBottom() {
        return cameraBoundsBottom;
    }

    /**
     * Divides the TiledMap's layers into specific, identifiable MapLayer objects.
     */
    public void setMapLayers()
    {
        lithology = (TiledMapTileLayer)section.getLayers().get("Lithology");
        fog = (TiledMapTileLayer)section.getLayers().get("Fog");
        activity = (TiledMapTileLayer)section.getLayers().get("Activity");
        character = (TiledMapTileLayer)section.getLayers().get("Character");
    }

    /**
     * Initializes the blocking state of the loaded section, setting rock to true and sky to false.
     * This is a temporary method to demonstrate fog of war and tunneling functionality
     * and is to be replaced in the future by an ore file that will be associated with the
     * .TMX map and generated separately via an ore generation utility.
     *
     * TO-DO: Remove.
     */
    public void initializeSectionUnits()
    {
        // Get section height and width
        int width = getSectionWidth();
        int height = getSectionHeight();

        // Initialize section unit array with height and width
        sectionUnit = new SectionUnit[height][width];

        // Cycle through the array
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                // Get the properties of the associated tile
                MapProperties properties = lithology.getCell(j, i).getTile().getProperties();
                boolean state = Boolean.parseBoolean(properties.get("state").toString());

                // Set the blocking state of the current tile
                sectionUnit[i][j] = new SectionUnit(state);
            }
        }

    }

    /**
     * Converts a rock unit into a tunnel.
     * @param x The x coordinate of the unit to change.
     * @param y The y coordinate of the unit to change.
     */
    public void convertUnitToTunnel(int x, int y)
    {
        // Set underlying rock unit solid/open state to open
        this.setLithologyBlock(x, y, false);

        // Add a new tunnel tile to the activity layer
        this.addNewTile(x, y, UnitType.Tunnel);

        // Delete fog of war
        removeFog(x, y);
    }

    /**
     * Adds a new tile to a map layer.
     * @param x The x coordinate position of the tile to add.
     * @param y The y coordinate position of the tile to add.
     * @param type The type of the tile to add.
     */
    public void addNewTile(int x, int y, UnitType type)
    {
        TiledMapTileLayer.Cell unit;

        switch (type)
        {
            // Add a vertical tunnel tile
            case Tunnel:

                // Create a texture from the tileset
                Texture tunnelTexture = new Texture(Gdx.files.internal("level_test.png"));

                // Specify a region within the tileset .PNG that represents the tile image
                TextureRegion tunnelRegion = new TextureRegion(tunnelTexture, 64, 128, 64, 64);

                // Create a new dummy Cell
                unit = new TiledMapTileLayer.Cell();

                // Initialize the cell with the tunnel region image
                unit.setTile(new StaticTiledMapTile(tunnelRegion));

                // Add cell to the activity layer at the specified location
                activity.setCell(x, y, unit);

                break;

            case Sky:

                // Create a texture from the tileset
                Texture skyTexture = new Texture(Gdx.files.internal("level_test.png"));

                // Specify a region within the tileset .PNG that represents the tile image
                TextureRegion skyRegion = new TextureRegion(skyTexture, 0, 0, 64, 64);

                // Create a new dummy Cell
                unit = new TiledMapTileLayer.Cell();

                // Initialize the cell with the tunnel region image
                unit.setTile(new StaticTiledMapTile(skyRegion));

                // Add cell to the activity layer at the specified location
                activity.setCell(x, y, unit);
        }
    }

    /**
     * Removes the fog of war around the newly added tile.
     * @param x The x location of the tile.
     * @param y The y location of the tile.
     */
    public void removeFog(int x, int y)
    {
        // Check to the left of the unit
        if (fog.getCell(x - 1, y) != null)
        {
            removeFogTile(fog.getCell(x - 1, y));
        }

        // Check to the upper left of the unit
        if (fog.getCell(x - 1, y + 1) != null)
        {
            removeFogTile(fog.getCell(x - 1, y + 1));
        }

        // Check above the unit
        if (fog.getCell(x, y + 1) != null)
        {
            removeFogTile(fog.getCell(x, y + 1));
        }

        // Check to the upper right of the unit
        if (fog.getCell(x + 1, y + 1) != null)
        {
            removeFogTile(fog.getCell(x + 1, y + 1));
        }

        // Check to the right of the unit
        if (fog.getCell(x + 1, y) != null)
        {
            removeFogTile(fog.getCell(x +1, y));
        }

        // Check to the lower right of the unit
        if (fog.getCell(x + 1, y - 1) != null)
        {
            removeFogTile(fog.getCell(x + 1, y - 1));
        }

        // Check below the unit
        if (fog.getCell(x, y - 1) != null)
        {
            removeFogTile(fog.getCell(x, y - 1));
        }

        // Check to the lower left of the unit
        if (fog.getCell(x - 1, y - 1) != null)
        {
            removeFogTile(fog.getCell(x - 1, y - 1));
        }
    }

    /**
     * Removes a specific fog of war unit.
     * @param fogUnit The unit of fog to remove.
     */
    public void removeFogTile(TiledMapTileLayer.Cell fogUnit)
    {
        // Remove cell by setting it to null
        fogUnit.setTile(null);

        // TO-DO
        // Update fog of war transparency edges
    }

    /**
     * Sets the blocking state of a unit to true or false (i.e. solid vs open).
     * @param x The x coordinate value of the cell to change.
     * @param y The y coordinate value of the cell to change.
     * @param blockingState The true or false value of the solid/open state.
     */
    public void setLithologyBlock(int x, int y, boolean blockingState)
    {
        // Set cell to blocking state
        sectionUnit[y][x].setState(blockingState);
    }

    /**
     * Gets the current blocking state of the cell.
     * @param x The x coordinate value of the cell to get.
     * @param y The y coordinate value of the cell to get.
     * @return The locking state.
     */
    public boolean getBlockingState(int x, int y)
    {
        boolean state = true;

        if (x == 0 || x > getSectionWidth() - 2 || y == 0)
        {
            state = false;
        }
        else
        {
            state = sectionUnit[y][x].getState();
        }
        return state;
    }

    /**
     * Gets the section unit at a specified location.
     * @param x The x coordinate of the unit.
     * @param y The y coordinate of the unit.
     * @return The section unit.
     */
    public SectionUnit getSectionUnit(int x, int y)
    {
        return sectionUnit[y][x];
    }


}
