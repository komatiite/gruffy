package com.komatiite.dunite;

/**
 * Created by kcroz on 2016-07-05.
 */
public class Upgrade
{
    public enum UpgradeType
    {
        Cost,
        Speed,
        Recovery
    }

    public UpgradeType type;
    public String name;
    public int price;
    public int value;
    public int level;
    public boolean active;


    /**
     * Construct the upgrade object.
     * @param type The type of upgrade: cost, speed, or recovery.
     * @param name The name of the upgrade.
     * @param price The price to buy the upgrade.
     * @param value The value of the benefit the upgrade provides.
     * @param level The level at which the upgrade unlocks.
     */
    public Upgrade(UpgradeType type, String name, int price, int value, int level)
    {
        this.type = type;
        this.name = name;
        this.price = price;
        this.value = value;
        this.level = level;
        this.active = false;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }
}
