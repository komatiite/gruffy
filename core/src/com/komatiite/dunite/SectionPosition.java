package com.komatiite.dunite;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;


/**
 * Creates a position object that holds the x and y unit coordinate values of the section.
 */
public class SectionPosition
{
    public int x;
    public int y;
    private Section section;

    /**
     * Constructs the section position object.
     * @param x The x position in pixels.
     * @param y The y position in pixels.
     * @param section The section to convert to.
     * @param camera The camera being used.
     */
    public SectionPosition(float x, float y, Section section, OrthographicCamera camera)
    {
        this.section = section;

        // Create a vector from the x and y coordinates
        Vector3 vector = new Vector3(x, y, 0);

        // Unproject the vector from the camera view
        camera.unproject(vector);

        // Convert pixel location to section location
        this.convertToSectionX(vector.x);
        this.convertToSectionY(vector.y);
    }

    /**
     * Converts the absolute pixel location of the x coordinate to the x section unit.
     * @param x The pixel x coordinate.
     */
    private void convertToSectionX(float x)
    {
        this.x = (int)x / section.getUnitWidth();
    }

    /**
     * Converts the absolute pixel location of the y coordinate to the y section unit.
     * @param y The pixel y coordinate.
     */
    public void convertToSectionY(float y)
    {
        this.y = (int)y / section.getUnitHeight();
    }
}