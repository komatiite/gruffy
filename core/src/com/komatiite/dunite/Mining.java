package com.komatiite.dunite;

import java.util.ArrayList;

/**
 * Performs mining behaviour on section units.
 */
public class Mining
{
    public enum Direction
    {
        Left,
        Right,
        Down
    }

    private static Tool currentTool;
    private static ArrayList<Node> minePath;
    private static int currentX;
    private static int currentY;

    /**
     * Checks the rock status of the section unit adjacent to the character.
     * @param section The section containing unit lithology information.
     * @param character The character who is intending to mine.
     * @param direction The direction in which the character wants to mine.
     * @return The true or false value of the unit state (rock = true, tunnel or sky = false).
     */
    public static boolean getMinePath(Section section, Character character, Direction direction)
    {
        minePath = new ArrayList<Node>();
        boolean hasPath = false;
        boolean makePath = true;
        int x = 0;
        int y = 0;

        Node node = new Node(null, character.getSectionX(), character.getSectionY(), 0, 0);

        minePath.add(node);

        for (int i = 0; i < 10 && makePath; i++)
        {
            // Switch between mining directions to get the appropriate section unit
            switch(direction)
            {
                case Left:
                    x = minePath.get(i).getX() - 1;
                    y = minePath.get(i).getY();
                    break;
                case Right:
                    x = minePath.get(i).getX() + 1;
                    y = minePath.get(i).getY();
                    break;
                case Down:
                    x = minePath.get(i).getX();
                    y = minePath.get(i).getY() - 1;
                    break;
            }

            if (!section.getBlockingState(x, y))
            {
                makePath = false;
            }
            else
            {
                minePath.add(new Node(minePath.get(i), x, y, 0, 0));

                hasPath = true;
            }
        }

        // Remove first node as it represents character's location
        minePath.remove(0);

        System.out.println("path size: " + minePath.size());

        return hasPath;
    }

    /**
     * Determines if a mine path exists.
     * @return The true or false value of whether or not a mine path exists.
     */
    public static boolean hasPath()
    {
        boolean path = false;

        if (minePath.size() > 0)
        {
            path = true;
        }

        return path;
    }

    /**
     * Sets the current tool to use in mining.
     * @param tool The tool to set as current.
     */
    public static void setTool(Tool tool)
    {
        currentTool = tool;
    }

    /**
     * Gets the currently-selected tool.
     * @return The current tool object.
     */
    public static Tool getTool()
    {
        return currentTool;
    }

    /**
     * Gets the x coordinate of the current mineable unit.
     * @return The integer x coordinate.
     */
    public static int getX()
    {
        return currentX;
    }

    /**
     * Gets the y coordinate of the current mineale unit.
     * @return The integer y coordinate.
     */
    public static int getY()
    {
        return currentY;
    }

    /**
     * Processes the current section unit for ore.
     * @param section The stratigraphic section.
     */
    public static void processOre(Section section)
    {
        // Set up coordinates
        currentX = minePath.get(0).getX();
        currentY = minePath.get(0).getY();

        // Get the unit to be processed
        SectionUnit unit = section.getSectionUnit(currentX, currentY);

        //System.out.println("Ore: " + unit.getGold() + " ************** " + getX() + ", " + getY());

        // After processing, convert section unit to a tunnel
        section.convertUnitToTunnel(currentX, currentY);

        // If there are more mining nodes, remove the current one
        if (minePath.size() > 0)
        {
            minePath.remove(0);
        }
    }
}
