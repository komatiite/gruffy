package com.komatiite.dunite.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.komatiite.dunite.Gruffy;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		// Set the configuration properties
		config.title = "Gruffy";
		config.useGL30 = false;
		config.width = 768;
		config.height = 1024;

		new LwjglApplication(new Gruffy(), config);
	}
}
